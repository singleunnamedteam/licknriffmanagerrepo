﻿using LickAndRiffManager.DAL.Entities;
using LickAndRiffManagerBLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.Interfaces
{
    public interface IEducationService
    {
        // Playlists
        List<PlaylistDTO> GetAllPlaylists();
        PlaylistDTO GetPlaylist(String id);
        bool AddPlaylist(PlaylistDTO item, ref String exceptionString);
        bool UpdatePlaylist(String id, PlaylistDTO playlist);
        List<LessonDTO> GetLessonsForPlaylist(String playlistId);
        PlaylistDTO FindPlaylistByName(String name);

        // Categories 
        List<CategoryDTO> GetAllCategories();
        CategoryDTO GetCategory(Int32 id);
        Int32 GetCategoryId(String name);
        bool AddCategory(CategoryDTO item, ref String exceptionString);
        bool DeleteCategory(Int32 id);
        bool UpdateCategory(Int32 id, CategoryDTO newCategory);
        bool AreEqual(CategoryDTO cat1, Category cat2);
        List<LessonDTO> GetAllLessonsForCategory(Int32 catId);

        // Lessons
        List<LessonDTO> GetAllLessons();
        LessonDTO FindLesson(String name);
        LessonDTO GetLesson(String id);
        bool AddLesson(LessonDTO item, ref String exceptionString);
        bool DeleteLesson(String id);
        bool UpdateLesson(String id, LessonDTO newLesson);
        bool ChangeStatus(String lessonId, Int32 newStatus);

        // Compositions
        List<CompositionDTO> GetAllCompositions();
        List<CompositionDTO> GetAllSongs();
        CompositionDTO FindComposition(String name);
        CompositionDTO GetComposition(Int32 id);
        bool AddComposition(CompositionDTO item, ref String exceptionString);
        bool DeleteComposition(Int32 id);
        bool UpdateComposition(Int32 id, CompositionDTO newComposition);
        bool ChangeCompositionStatus(Int32 compositionId, Int32 newStatus);
    }
}
