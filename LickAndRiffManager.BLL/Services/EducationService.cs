﻿using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LickAndRiffManagerBLL.DTO;
using LickAndRiffManager.DAL.Util;
using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Entities.Enums;

namespace LickAndRiffManagerBLL.Services
{
    public class EducationService : IEducationService
    {
        UnitOfWork uoW;

        public EducationService()
        {
            uoW = new UnitOfWork();
        }

        public EducationService(String connectionString)
        {
            uoW = new UnitOfWork(connectionString);
        }

        #region Categories
        // Categories
        public bool AddCategory(CategoryDTO item, ref String exceptionMessage)
        {
            // Validation
            if (item == null)
            {
                exceptionMessage = "Item is null";
                return false;
            }

            Category newCat = new Category()
            {
                Name = item.Name
            };

            bool result = uoW.Categories.Add(newCat, ref exceptionMessage);

            uoW.Save();

            return result;
        }

        public bool DeleteCategory(int id)
        {
            bool result = uoW.Categories.Delete(id);

            uoW.Save();
            return result;
        }

        public Int32 GetCategoryId(String name)
        {
            var category = uoW.Categories.Find(c => c.Name == name).FirstOrDefault();

            if (category == null)
            {
                return -1;
            }

            return category.CategoryId;
        }

        public List<CategoryDTO> GetAllCategories()
        {
            return uoW.Categories.GetAll().Select(t => new CategoryDTO(t)).ToList();
        }

        public CategoryDTO GetCategory(int id)
        {
            Category cat = uoW.Categories.Get(id);

            if (cat == null)
            {
                return null;
            }

            return new CategoryDTO(cat);
        }

        public bool UpdateCategory(int id, CategoryDTO newCategory)
        {
            bool result = uoW.Categories.Update(id, new Category()
            {
                Name = newCategory.Name
            });
            uoW.Save();

            return result;
        }
        // Empty
        public bool AreEqual(CategoryDTO cat1, Category cat2)
        {
            return false;
        }
        #endregion

        #region Lessons
        // Lessons
        public bool AddLesson(LessonDTO item, ref String exceptionMessage)
        {
            // Validation
            if (item == null)
            {
                exceptionMessage = "Item is null";
                return false;
            }

            if (item.LessonId == null)
            {
                do
                {
                    item.LessonId = Guid.NewGuid().ToString();
                }
                while (uoW.Lessons.Get(item.LessonId) != null);
            }

            Lesson newLes = new Lesson()
            {
                Duration = item.Duration,
                Link = item.Link,
                CategoryId = item.CategoryId,
                PlaylistId = item.PlaylistId,
                Status = (StatusEnum)item.Status,
                Name = item.Name,
                LessonId = item.LessonId,
                Position = item.Position
            };

            bool addingResult = uoW.Lessons.Add(newLes, ref exceptionMessage);

            uoW.Save();

            return addingResult;
        }

        public bool DeleteLesson(String id)
        {
            bool result = uoW.Lessons.Delete(id);
            uoW.Save();

            return result;
        }

        public List<LessonDTO> GetAllLessons()
        {
            return uoW.Lessons.GetAll().Select(t => new LessonDTO(t)).ToList();
        }

        public LessonDTO GetLesson(String id)
        {
            Lesson cat = uoW.Lessons.Get(id);

            if (cat == null)
            {
                return null;
            }

            return new LessonDTO(cat);
        }

        public LessonDTO FindLesson(String name)
        {
            Lesson less = uoW.Lessons.Find(l => l.Name == name).SingleOrDefault();

            if (less == null)
            {
                return null;
            }

            return new LessonDTO(less);
        }

        public bool UpdateLesson(String id, LessonDTO newLesson)
        {
            bool result = uoW.Lessons.Update(id, new Lesson()
            {
                Duration = newLesson.Duration,
                Link = newLesson.Link,
                CategoryId = newLesson.CategoryId,
                PlaylistId = newLesson.PlaylistId,
                Status = (StatusEnum)newLesson.Status
            });
            uoW.Save();

            return result;
        }

        public List<LessonDTO> GetAllLessonsForCategory(int catId)
        {
            return uoW.Lessons.Find(t => t.CategoryId == catId).Select(t => new LessonDTO(t)).ToList();
        }

        public bool ChangeStatus(String lessonId, int newStatus)
        {
            StatusEnum newStat = (StatusEnum)newStatus;

            bool result = true;

            Lesson oldLesson = uoW.Lessons.Get(lessonId);

            result = uoW.Lessons.Update(lessonId, new Lesson()
            {
                Duration = oldLesson.Duration,
                Name = oldLesson.Name,
                Status = newStat
            });

            uoW.Save();

            return result;
        }
        #endregion

        #region Playlists
        // Playlists section

        public List<PlaylistDTO> GetAllPlaylists()
        {
            return uoW.Playlists.GetAll().Select(p => new PlaylistDTO(p)).ToList();
        }

        public PlaylistDTO GetPlaylist(String id)
        {
            Playlist playlist = uoW.Playlists.Get(id);

            if (playlist == null)
            {
                return null;
            }

            return new PlaylistDTO(playlist);
        }

        public bool AddPlaylist(PlaylistDTO item, ref string exceptionString)
        {
            // Validation
            if (item == null)
            {
                exceptionString = "Item is null";
                return false;
            }

            Playlist newPlaylist = new Playlist()
            {
                PlaylistId = item.PlaylistId,
                Name = item.Name,
            };

            bool addingResult = uoW.Playlists.Add(newPlaylist, ref exceptionString);

            uoW.Save();

            return addingResult;
        }

        public bool UpdatePlaylist(String id, PlaylistDTO newPlaylist)
        {
            bool result = uoW.Playlists.Update(id, new Playlist() {
                Name = newPlaylist.Name
                // Link is immutable
            });

            uoW.Save();

            return result;
        }

        public List<LessonDTO> GetLessonsForPlaylist(String playlistId)
        {
            return uoW.Lessons.Find(l => l.PlaylistId == playlistId)
                              .Select(l => new LessonDTO(l))
                              .ToList();
        }

        public PlaylistDTO FindPlaylistByName(string name)
        {
            var playList = uoW.Playlists.Find(p => p.Name == name)
                                                .FirstOrDefault();

            if (playList == null)
            {
                return null;
            }
            else
            {
                return new PlaylistDTO(playList);
            }
        }
        #endregion

        // General section
        public void Dispose()
        {
            uoW.Dispose();
        }

        #region Compositions
        // Composition section
        public List<CompositionDTO> GetAllCompositions()
        {
            return uoW.Compositions.GetAll().Where(c => c.Kind == KindEnum.COMPOSITION)
                                            .Select(c => new CompositionDTO(c)).ToList();
        }

        public List<CompositionDTO> GetAllSongs()
        {
            return uoW.Compositions.GetAll().Where(c => c.Kind == KindEnum.SONG)
                                            .Select(c => new CompositionDTO(c)).ToList();
        }

        public CompositionDTO FindComposition(string name)
        {
            Composition com = uoW.Compositions.Find(c => c.Name == name).FirstOrDefault();
            if (com == null)
            {
                return null;
            }

            return new CompositionDTO(com);
        }

        public CompositionDTO GetComposition(int id)
        {
            Composition com = uoW.Compositions.Get(id);
            if (com == null)
            {
                return null;
            }

            return new CompositionDTO(com);
        }

        public bool AddComposition(CompositionDTO item, ref string exceptionString)
        {
            // Validation
            if (item == null)
            {
                exceptionString = "Item is null";
                return false;
            }

            Composition newComp = new Composition()
            {
                Link = item.Link,
                Name = item.Name,
                Kind = (KindEnum)item.Kind,
                Status = (StatusEnum)item.Status
            };

            bool addingResult = uoW.Compositions.Add(newComp, ref exceptionString);

            uoW.Save();

            return addingResult;
        }

        public bool DeleteComposition(Int32 id)
        {
            bool result = uoW.Compositions.Delete(id);
            uoW.Save();

            return result;
        }

        public bool UpdateComposition(Int32 id, CompositionDTO newComposition)
        {
            bool result = uoW.Compositions.Update(id, new Composition()
            {
                Link = newComposition.Link,
                Name = newComposition.Name,
                Status = (StatusEnum)newComposition.Status
            });
            uoW.Save();

            return result;
        }

        public bool ChangeCompositionStatus(Int32 compositionId, int newStatus)
        {
            StatusEnum newStat = (StatusEnum)newStatus;

            bool result = true;

            Composition oldComposition = uoW.Compositions.Get(compositionId);

            result = uoW.Compositions.Update(compositionId, new Composition()
            {
                CompositionId = oldComposition.CompositionId,
                Name = oldComposition.Name,
                Link = oldComposition.Link,
                Status = newStat
            });

            uoW.Save();

            return result;
        }
        #endregion
    }
}
