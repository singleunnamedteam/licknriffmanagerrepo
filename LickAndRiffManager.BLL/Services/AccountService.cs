﻿using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Util;
using LickAndRiffManagerBLL.DTO;
using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.Services
{
    class AccountService: IAccountService
    {
        UnitOfWork uoW;

        public AccountService()
        {
            uoW = new UnitOfWork();
        }

        public AccountService(String connectionString)
        {
            uoW = new UnitOfWork(connectionString);
        }

        public void Dispose()
        {
            uoW.Dispose();
        }
    }
}
