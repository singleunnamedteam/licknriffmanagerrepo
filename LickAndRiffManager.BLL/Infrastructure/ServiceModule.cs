﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using LickAndRiffManagerBLL.Services;
using LickAndRiffManagerBLL.Interfaces;
using LickAndRiffManagerBLL.BusinessModels;

namespace LickAndRiffManagerBLL.Infrastructure
{
    public static class ServiceModule
    {        
        public static ContainerBuilder GetBuilder(String connectionString)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<EducationService>().As<IEducationService>()
                                                   .WithParameter("connectionString", connectionString);
            builder.RegisterType<AccountService>().As<IAccountService>()
                                                 .WithParameter("connectionString", connectionString);

            builder.RegisterType<DBUpdateManager>().AsSelf()
                                                   .WithParameter("connectionString", connectionString);
            return builder;
        }       
    }
}
