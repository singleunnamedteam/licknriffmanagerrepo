﻿using LickAndRiffManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.DTO
{
    public class PlaylistDTO
    {
        public PlaylistDTO(Playlist playlist)
        {
            PlaylistId = playlist.PlaylistId;
            Name = playlist.Name;
        }

        public PlaylistDTO()
        {
        }

        public String PlaylistId { get; set; }
        public String Name { get; set; }
    }
}
