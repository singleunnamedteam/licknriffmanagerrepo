﻿using LickAndRiffManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.DTO
{
    public class CategoryDTO
    {
        public CategoryDTO()
        {
        }

        public CategoryDTO(Category category)
        {
            Id = category.CategoryId;
            Name = category.Name;
        }

        public Int32 Id { get; set; }
        public String Name { get; set; }
    }
}
