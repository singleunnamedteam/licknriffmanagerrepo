﻿using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.DTO
{
    public class LessonDTO
    {
        public LessonDTO()
        {

        }

        public LessonDTO(Lesson lesson)
        {
            LessonId = lesson.LessonId;
            Name = lesson.Name;
            Duration = lesson.Duration;
            Link = lesson.Link;
            Status = (Int32)lesson.Status;
            PlaylistId = lesson.PlaylistId;
            Position = lesson.Position;
        }

        public String LessonId { get; set; }
        public String Name { get; set; }
        public Int32? CategoryId { get; set; }
        public Int32 Duration { get; set; }
        public String Link { get; set; }
        public Int32 Status { get; set; }
        public String PlaylistId { get; set; }
        public Int32 Position { get; set; }
    }
}
