﻿using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.DTO
{
    public class CompositionDTO
    {
        public Int32 CompositionId { get; set; }
        public String Name { get; set; }
        public String Link { get; set; }
        public Int32 Status { get; set; }
        public Byte Kind { get; set; }

        public CompositionDTO(Composition compos)
        {
            CompositionId = compos.CompositionId;
            Name = compos.Name;
            Link = compos.Link;
            Status = (Int32)compos.Status;
            Kind = (Byte)compos.Kind;
        }

        public CompositionDTO()
        {

        }
    }
}
