﻿using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Entities.Enums;
using LickAndRiffManager.DAL.Util;
using LickAndRiffManagerBLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManagerBLL.BusinessModels
{
    public class DBUpdateManager
    {
        private UnitOfWork uoW;

        public DBUpdateManager()
        {
            uoW = new UnitOfWork();
        }

        public DBUpdateManager(String connectionString)
        {
            uoW = new UnitOfWork(connectionString);
        }

        public bool UpdateLessons(ICollection<LessonDTO> lessons)
        {
            bool result = true;
            foreach (var lesson in lessons)
            {
                Lesson lessonInDB = uoW.Lessons.Get(lesson.LessonId);
                if (lessonInDB == null)
                {
                    String exceptionMessage = "";
                    bool addResult = uoW.Lessons.Add(new Lesson()
                    {
                        Link = lesson.Link,
                        Name = lesson.Name,
                        CategoryId = lesson.CategoryId,
                        Status = 0,
                        Duration = lesson.Duration,
                        PlaylistId = lesson.PlaylistId,
                        LessonId = lesson.LessonId,
                        Position = lesson.Position
                    }, ref exceptionMessage);

                    if (addResult == false)
                    {
                        result = false;
                    }
                    uoW.Save();
                }
                else
                {
                    uoW.Save();
                    bool updateRes = true;
                    if (!AreEqual(lesson, lessonInDB))
                    {
                        updateRes = uoW.Lessons.Update(lessonInDB.LessonId, new Lesson()
                        {
                            Link = lessonInDB.Link,
                            Name = lesson.Name,
                            CategoryId = lessonInDB.CategoryId,
                            Status = lessonInDB.Status,
                            Duration = lesson.Duration,
                            Position = lesson.Position
                        });
                    }

                    if (!updateRes)
                    {
                        result = false;
                    }
                    uoW.Save();
                }                
            }

            uoW.Save();
            return result;
        }

        public bool UpdatePlaylists(ICollection<PlaylistDTO> playlists)
        {
            bool result = true;

            foreach (var p in playlists)
            {
                Playlist pInDB = uoW.Playlists.Get(p.PlaylistId);
                if (pInDB == null)
                {
                    String exceptionMessage = "";
                    bool addResult = uoW.Playlists.Add(new Playlist()
                    {
                        PlaylistId = p.PlaylistId,
                        Name = p.Name
                    }, ref exceptionMessage);

                    if (addResult == false)
                    {
                        result = false;
                    }
                }
                else
                {
                    uoW.Save();
                    bool updateRes = true;
                    if (!AreEqual(p, pInDB))
                    {
                        updateRes = uoW.Playlists.Update(pInDB.PlaylistId, new Playlist()
                        {
                            Name = p.Name
                        });
                        uoW.Save();
                    }

                    if (!updateRes)
                    {
                        result = false;
                    }
                    uoW.Save();
                }
            }

            uoW.Save();
            return result;
        }

        protected bool AreEqual(LessonDTO lesson1, Lesson lesson2)
        {
            if (lesson1 == null || lesson2 == null)
            {
                return false;
            }

            if (lesson1.Duration != lesson2.Duration ||
                lesson1.Link != lesson2.Link ||
                lesson1.Name != lesson2.Name || 
                lesson1.Position != lesson2.Position ||
                (lesson1.PlaylistId != lesson2.PlaylistId && lesson1.PlaylistId == "PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr"))
            {
                return false;
            }

            return true;
        }

        protected bool AreEqual(PlaylistDTO p1, Playlist p2)
        {
            if (p1.Name == p2.Name)
            {
                return true;
            }

            return false;
        }
    }
}
