﻿using Autofac;
using LickAndRiffManager.Util;
using LickAndRiffManagerBLL.BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LickAndRiffManager.Modules
{
    public class TimerModule : IHttpModule
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static Timer timer;
        private long interval = 1000 * 60 * 60 * 100; 
        private static object synclock = new object();
        DBUpdateManager manager = AutofacConfig.GetContainer().Resolve<DBUpdateManager>();

        public void Init(HttpApplication app)
        {
            timer = new Timer(new TimerCallback(GetDataFromYoutube), null, 0, interval);
        }

        private void GetDataFromYoutube(object obj)
        {
            YoutubeManager youtube = new YoutubeManager();

            lock (synclock)
            {
                DateTime dd = DateTime.Now;
                _logger.Info("Updating started");
                bool result1 = true;
                bool result2 = true;

                //try
                //{
                //    var playlists = youtube.GetPlaylists();
                //    result1 = manager.UpdatePlaylists(playlists);
                //}
                //catch (System.Data.Entity.Infrastructure.DbUpdateException)
                //{
                //    result1 = false;
                //}
                var lessons = youtube.GetLessons();
                result2 = manager.UpdateLessons(lessons);
                //}
                //catch (System.Data.Entity.Infrastructure.DbUpdateException)
                //{
                ////    result2 = false;
                //}

                if (result1 && result2)
                {
                    _logger.Info("Updating has been finished");
                }
                else
                {
                    _logger.Info("Updating has failed" + "res1" + result1.ToString() + "res2:" + result2.ToString());
                }
            }
        }
        public void Dispose()
        { }
    }
}
