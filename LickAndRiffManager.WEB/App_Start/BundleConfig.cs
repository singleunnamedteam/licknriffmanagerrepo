﻿using System.Web;
using System.Web.Optimization;

namespace LickAndRiffManager
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap_optimizer.css",
                      "~/Content/position.css",
                      "~/Content/tooltip.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include("~/Scripts/angular.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-bootstrap")
                   .Include("~/Scripts/angular/angular-bootstrap-ui/collapse.js",
                            "~/Scripts/angular/angular-bootstrap-ui/position.js",
                            "~/Scripts/angular/angular-bootstrap-ui/stackedMap.js",
                            "~/Scripts/angular/angular-bootstrap-ui/tooltip.js",
                            "~/Scripts/angular/angular-bootstrap-ui/modal.js"));

            bundles.Add(new ScriptBundle("~/bundles/my-angular-components")
                   .Include("~/Scripts/angular/modules/lickNRiffManagerModule.js",
                            "~/Scripts/angular/services/mainService.js"));

            bundles.Add(new ScriptBundle("~/bundles/categories-components")
                   .Include("~/Scripts/angular/filters/filter.js",
                            "~/Scripts/angular/controllers/addLessonModalController.js",
                            "~/Scripts/angular/controllers/mainController.js",
                            "~/Scripts/angular/directives/categoryBlock.js"));

            bundles.Add(new ScriptBundle("~/bundles/lessons-only-components")
                   .Include("~/Scripts/angular/filters/filter.js",
                            "~/Scripts/angular/controllers/mainLessonsController.js",
                                "~/Scripts/angular/directives/lessonBlock.js"));

            bundles.Add(new ScriptBundle("~/bundles/compositions-only-components")
                        .Include("~/Scripts/angular/controllers/addCompositionModalController.js",
                        "~/Scripts/angular/controllers/mainCompositionsController.js",
                        "~/Scripts/angular/directives/compositionBlock.js"));

            bundles.Add(new ScriptBundle("~/bundles/songs-only-components")
            .Include("~/Scripts/angular/controllers/addSongModalController.js",
            "~/Scripts/angular/controllers/mainSongsController.js",
            "~/Scripts/angular/directives/compositionBlock.js"));
        }
    }
}
