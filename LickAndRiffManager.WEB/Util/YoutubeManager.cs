﻿using LickAndRiffManagerBLL.DTO;
using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;

namespace LickAndRiffManager.Util
{
    public sealed class YoutubeManager
    {
        private String baseUrl = "https://www.googleapis.com/youtube/v3/";

        private List<String> playlistsIds = new List<String>()
        {
            "PLiYP5lAo2JmzqSqGwT-MJ_ZPTLbJATklR",
            "PLiYP5lAo2JmzR-yQfn5o4JidQSfWqPjPp",
            "PLiYP5lAo2JmwbN-xvBfj2ts_DTWA7OHYc",
            "PLiYP5lAo2JmySUJ1s4LYwQSEwz6nLi_bL"
        };
        IEducationService eduService = AutofacConfig.GetContainer().Resolve<IEducationService>();

        public List<PlaylistDTO> GetPlaylists()
        {
            //String url = baseUrl + "playlists?part=snippet&channelId=UC_tejXLLDBrLyZFk2cZrhHw&fields=items(id%2Csnippet%2Ftitle)%2CnextPageToken&key=AIzaSyArliaDqTD4CFs7ZIYry_TZ1M_xzjpdokw";
            //const String pageTokenStart = "&pageToken=";
            //String nextPageToken = null;
            //var ids = new List<String>();
            //var names = new List<String>();


            //while (nextPageToken != "")
            //{
            //    WebRequest request;

            //    Getting data from youtube
            //    if (nextPageToken == null)
            //    {
            //        request = WebRequest.Create(url);
            //    }
            //    else
            //    {
            //        request = WebRequest.Create(url + pageTokenStart + nextPageToken);
            //    }

            //    WebResponse response = request.GetResponse();

            //    Converting it to String
            //    StringBuilder json = new StringBuilder();
            //    using (Stream stream = response.GetResponseStream())
            //    {
            //        using (StreamReader reader = new StreamReader(stream))
            //        {
            //            String line = "";
            //            while ((line = reader.ReadLine()) != null)
            //            {
            //                json.Append(line);
            //            }
            //        }
            //    }

            //    Closing response
            //    response.Close();

            //    Constructing regexp
            //    String jsonStr = json.ToString();

            //    nextPageToken = Regex.Match(jsonStr, "\"nextPageToken\": \"\\S+\"").Value;
            //    if (nextPageToken != "")
            //    {
            //        nextPageToken = nextPageToken.Split('\"')[3];
            //    }

            //    foreach (Match match in Regex.Matches(jsonStr, "\"id\": \"\\S+\","))
            //    {
            //        ids.Add(match.Value.Split('\"')[3]);
            //    }

            //    foreach (Match match in Regex.Matches(jsonStr, "\"title\": \"[^\"]+\""))
            //    {
            //        String matchStr = match.Value.Split('\"')[3];
            //        if (!names.Exists(n => n == matchStr))
            //        {
            //            names.Add(matchStr);
            //        }
            //    }
       // }

       // Saving playList ids
       //playlistsIds = ids;

       // Formatting result
       //     List<PlaylistDTO> result = ids.Select(p => new PlaylistDTO()
       //     {
       //         PlaylistId = p
       //     }).ToList();

       //     for (Int32 i = 0; i<ids.Count; i++)
       //     {
       //         result[i].Name = names[i];
       //     }

            return eduService.GetAllPlaylists();
        }

        public List<LessonDTO> GetLessons()
        {
            var result = new List<LessonDTO>();

            //if (playlistsIds == null)
            //{
            //    playlistsIds = GetPlaylists().Select(t => t.PlaylistId)
            //                                 .ToList();
            //}

            // 1. Getting lessons without duration and category
            #region lesons_without_duration_and_category

            const String pageTokenStart = "&pageToken=";
            Int32 initialLength = 0;
            for (Int32 i = 0; i < playlistsIds.Count(); i++)
            {
                String url = baseUrl + "playlistItems?part=snippet&playlistId=" + playlistsIds[i] + "&fields=items(snippet(position%2CresourceId%2FvideoId%2Ctitle))%2CnextPageToken&key=AIzaSyArliaDqTD4CFs7ZIYry_TZ1M_xzjpdokw";
                String nextPageToken = null;
                var ids = new List<String>();
                var names = new List<String>();
                var positions = new List<Int32>();

                while (nextPageToken != "")
                {
                    WebRequest request;

                    // Getting data from youtube
                    if (nextPageToken == null)
                    {
                        request = WebRequest.Create(url);
                    }
                    else
                    {
                        request = WebRequest.Create(url + pageTokenStart + nextPageToken);
                    }

                    WebResponse response = request.GetResponse();

                    // Converting it to String
                    StringBuilder json = new StringBuilder();
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            String line = "";
                            while ((line = reader.ReadLine()) != null)
                            {
                                json.Append(line);
                            }
                        }
                    }

                    // Closing response
                    response.Close();

                    // Constructing regexp
                    String jsonStr = json.ToString();
                    jsonStr = jsonStr.Replace("\",", "\",\n");

                    nextPageToken = Regex.Match(jsonStr, "\"nextPageToken\": \"\\S+\"").Value;
                    if (nextPageToken != "")
                    {
                        nextPageToken = nextPageToken.Split('\"')[3];
                    }

                    foreach (Match match in Regex.Matches(jsonStr, "\"videoId\": \"\\S+\""))
                    {
                        ids.Add(match.Value.Split('\"')[3]);
                    }

                    foreach (Match match in Regex.Matches(jsonStr, "\"title\": \".+\n"))
                    {
                        String tempTitle = match.Value.Split('\"')[3];
                        names.Add(tempTitle);
                    }

                    foreach (Match match in Regex.Matches(jsonStr, "position\\\": [0-9]+"))
                    {
                        Int32 position = Convert.ToInt32(match.Value.Split(':')[1]
                                            .Substring(1));
                        positions.Add(position);
                    }
                }

                // Formatting result without Category and duration
                result.AddRange(ids.Select(p => new LessonDTO()
                {
                    LessonId = p
                }).ToList());

                for (Int32 j = initialLength; j < initialLength + ids.Count; j++)
                {
                    result[j].Name = names[j - initialLength];
                    result[j].Position = positions[j - initialLength];
                    result[j].Link = "https://www.youtube.com/watch?v=" + result[j].LessonId;
                    result[j].PlaylistId = "PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr";
                    result[j].Status = 0;
                }

                initialLength += ids.Count();
            }

            #endregion

            // 2. Getting duration for each lesson and choosing categories
            #region getting_duration_and_category
            foreach (var lesson in result)
            {
                var videoUrl = "https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=" + lesson.LessonId + "&fields=items%2FcontentDetails%2Fduration&key=AIzaSyArliaDqTD4CFs7ZIYry_TZ1M_xzjpdokw";
                WebRequest request = WebRequest.Create(videoUrl);

                WebResponse response = request.GetResponse();

                // Converting it to String
                StringBuilder json = new StringBuilder();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        String line = "";
                        while ((line = reader.ReadLine()) != null)
                        {
                            json.Append(line);
                        }
                    }
                }

                // Closing response
                response.Close();

                //// Constructing regexp
                //String jsonStr = json.ToString();
                //String duration = Regex.Match(jsonStr, "\"duration\": \"\\S+\"").Value.Split('\"')[3];
                //Int32 hours = 0;
                //if (duration.Contains("H"))
                //{
                //    Convert.ToInt32(Regex.Match(duration, "\\d+H").Value.Replace("H", ""));
                //}

                //Int32 minutes = 0;
                //if (duration.Contains("M"))
                //{
                //    minutes = Convert.ToInt32(Regex.Match(duration, "\\d+M").Value.Replace("M", ""));
                //}

                //Int32 seconds = 0;
                //if (duration.Contains("S"))
                //{
                //    seconds = Convert.ToInt32(Regex.Match(duration, "\\d+S").Value.Replace("S", ""));
                //}

                //lesson.Duration = hours * 3600 + minutes * 60 + seconds;

                if (lesson.PlaylistId == "PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr")
                {
                    lesson.CategoryId = ChooseCategoryId(lesson.Name, lesson.LessonId);
                }
                else
                {
                    lesson.CategoryId = null;
                }
            }

            #endregion


            Regex regex1 = new Regex("Tutorial");
            Regex regex2 = new Regex("w/ TAB");
            Regex regex3 = new Regex("Tutorial w/ TAB");

            LessonDTO res = null;
            while ((res = result.Find(l => (regex1.IsMatch(l.Name) || regex2.IsMatch(l.Name)) && !regex3.IsMatch(l.Name) ||
                                    l.Name == "Guitar Tutorial: Mexican Hat Dance" || l.Name == "Deleted video")) != null)
            {
                result.Remove(res);
            }

            return result;
        }
         
        private Int32 ChooseCategoryId(String name, String id)
        {
            if (name.Contains("Percus"))
            {
                return eduService.GetCategoryId("Percussion");
            }

            if (name.Contains("Flamenco") || name.Contains("Bossa Nova") || name.Contains("Spanish"))
            {
                return eduService.GetCategoryId("Spanish/Flamenco");
            }

            if (name.Contains("Travis Picking"))
            {
                return eduService.GetCategoryId("Travis Picking");
            }

            if (name.Contains("Blues"))
            {
                return eduService.GetCategoryId("Blues and rock & roll");
            }

            if (name.Contains("Country"))
            {
                return eduService.GetCategoryId("Country");
            }

            if (name.Contains("Obstacles") || name.Contains("One True Secret") ||
                name.Contains("Ruin") || name.Contains("Frustration"))
            {
                return eduService.GetCategoryId("Psychology");
            }

            if (name.Contains("Improv"))
            {
                return eduService.GetCategoryId("Improvising");
            }

            if (name.Contains("Harmonics"))
            {
                return eduService.GetCategoryId("Harmonics");
            }

            if (name.Contains("Solo"))
            {
                return eduService.GetCategoryId("Soloing");
            }

            if (name.Contains("Harmonica"))
            {
                return eduService.GetCategoryId("Harmonica");
            }

            if (name.Contains("Exerc") || name.Contains("Beginner") || name.Contains("Advanc") || name.Contains("Interm"))
            {
                return eduService.GetCategoryId("Exercises");
            }

            return eduService.GetCategoryId("Other");
        }
    }
}
