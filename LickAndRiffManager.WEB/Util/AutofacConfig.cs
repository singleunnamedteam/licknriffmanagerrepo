﻿using Autofac;
using LickAndRiffManagerBLL.Infrastructure;
using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.Util
{
    internal static class AutofacConfig
    {
        private static IContainer container;

        static AutofacConfig()
        {
            string connStr = ConfigurationManager.ConnectionStrings["LickNRiffCon"].ConnectionString;
            var builder = ServiceModule.GetBuilder(connStr);
            container = builder.Build();
        }

        internal static IContainer GetContainer()
        {
            return container;
        }
    }
}
