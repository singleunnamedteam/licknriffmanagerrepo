﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LickAndRiffManager.Models;
using LickAndRiffManagerBLL.Interfaces;
using LickAndRiffManagerBLL.DTO;
using LickAndRiffManagerBLL.Infrastructure;
using System.Configuration;
using Autofac;

using LickAndRiffManager.Util;

namespace LickAndRiffManager.Controllers
{
    public sealed class CategoriesController : ApiController
    {
        private IEducationService eduService = AutofacConfig.GetContainer()
                                                            .Resolve<IEducationService>();

        // GET api/<controller>
        [HttpGet]
        public IHttpActionResult GetMainModel()
        {
            CategoriesLessonsMainViewModel model = new CategoriesLessonsMainViewModel();

            model.PlaylistId = eduService.GetPlaylist("PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr")
                                         .PlaylistId;

            model.Categories = new List<CategoryViewModel>();

            List<CategoryDTO> categories = eduService.GetAllCategories();
            
            if (categories == null)
            {
                return InternalServerError();
            }

            model.Categories.AddRange(categories.Select(t => new CategoryViewModel()
            {
                Id = t.Id,
                Name = t.Name,
                Lessons = eduService.GetAllLessonsForCategory(t.Id).OrderBy(l => l.Position).ToList(),
            }).Where(cat => cat.Lessons.Count != 0));

            if (model.Categories.Count == 0)
            {
                return BadRequest("No lessons in this playlist");
            }

            foreach (var cat in model.Categories)
            {
                cat.TotalDuration = cat.Lessons.Sum(t => t.Duration);
                cat.FinishedDuration = cat.Lessons.Where(l => l.Status == 3)
                                          .Sum(l => l.Duration);
                cat.CountOfFinished = cat.Lessons.Count(l => l.Status == 3);
                cat.CountOfLessons = cat.Lessons.Count();

                cat.CountOfPlanned = cat.Lessons.Count(l => l.Status == 2);
            }

            return Json(model);
        }

        //[HttpGet]
        //public IHttpActionResult GetAllCategories()
        //{
        //    return Json(eduService.GetAllCategories()
        //                          .Select(c => new CategoryViewModel()
        //    {
        //        FinishedDuration = 0,

        //    }))
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}