﻿using LickAndRiffManager.Util;
using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Autofac;
using System.Web.Http;
using LickAndRiffManagerBLL.DTO;
using System.Web.Http.ModelBinding;

namespace LickAndRiffManager.Controllers
{
    public class CompositionsController : ApiController
    {
        private IEducationService eduService = AutofacConfig.GetContainer()
                                             .Resolve<IEducationService>();

        // Get all
        [HttpGet]
        public IHttpActionResult Get(String kind)
        {
            if (kind == "SONGS")
            {
                return Json(eduService.GetAllSongs());
            }
            else
            {
                return Json(eduService.GetAllCompositions());
            }
        }

        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult PostStatus(Int32 compositionId, Int32 newStatus)
        {
            bool result = eduService.ChangeCompositionStatus(compositionId, newStatus);

            if (result == false)
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Post(CompositionDTO model)
        {
            String exceptionStr = null;
            bool res = eduService.AddComposition(model, ref exceptionStr);
            
            if (res)
            {
                return Json(eduService.FindComposition(model.Name).CompositionId);
            }
            else
            {
                return Json(exceptionStr);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Int32 id)
        {
            return Json(eduService.DeleteComposition(id));
        }
    }
}
