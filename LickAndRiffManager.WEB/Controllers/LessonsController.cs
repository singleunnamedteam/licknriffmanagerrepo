﻿using Autofac;
using LickAndRiffManager.Util;
using LickAndRiffManagerBLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using LickAndRiffManagerBLL.DTO;

namespace LickAndRiffManager.Controllers
{
    public class LessonsController : ApiController
    {
        private IEducationService eduService = AutofacConfig.GetContainer()
                                                .Resolve<IEducationService>();
        // GET api/<controller>
        [HttpGet]
        public IHttpActionResult Get(String id)
        {
            return Json(eduService.GetLessonsForPlaylist(id).OrderBy(l => l.Position));
        }

        // POST api/<controller>
        [HttpPost]
        public IHttpActionResult PostStatus(String lessonId, Int32 newStatus)
        {
            bool result = eduService.ChangeStatus(lessonId, newStatus);

            if (result == false)
            {
                return InternalServerError();
            }

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult Post(LessonDTO model)
        {
            String exceptionStr = null;
            model.Duration = 0;
            model.Position = 500;
            bool res = eduService.AddLesson(model, ref exceptionStr);

            if (res)
            {
                return Json(new { id = eduService.FindLesson(model.Name).LessonId });
            }
            else
            {
                return Json(exceptionStr);
            }
        }

        [HttpDelete]
        public bool Delete(String id)
        {
            return eduService.DeleteLesson(id);
        }
    }
}
