﻿using Autofac;
using LickAndRiffManager.Models;
using LickAndRiffManager.Util;
using LickAndRiffManagerBLL.DTO;
using LickAndRiffManagerBLL.Interfaces;
using LickAndRiffManagerBLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LickAndRiffManager.Controllers
{
    public class HomeController : Controller
    {
        IEducationService eduService = AutofacConfig.GetContainer()
                                            .Resolve<IEducationService>();

        public ActionResult Index()
        {
            try
            {
                PlaylistsMainModel model = new PlaylistsMainModel();
                model.Playlists = eduService.GetAllPlaylists();

                if (model.Playlists.Count == 0)
                {
                    model.ActivePlaylistId = "";
                }
                else
                {
                    model.ActivePlaylistId = model.Playlists
                                                  .Where(p => p.PlaylistId == "PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr")
                                                  .First().PlaylistId;
                }

                return View(model);
            }
            catch(Exception exp)
            {
                return Json(exp.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Playlists(String id)
        {
            PlaylistsMainModel model = new PlaylistsMainModel();

            model.Playlists = eduService.GetAllPlaylists();

            model.ActivePlaylistId = id;

            return View(model);
        }

        public ActionResult Compositions(String id)
        {
            PlaylistsMainModel model = new PlaylistsMainModel();

            model.Playlists = eduService.GetAllPlaylists();

            model.ActivePlaylistId = id;

            return View(model);
        }

        public ActionResult Songs(String id)
        {
            PlaylistsMainModel model = new PlaylistsMainModel();

            model.Playlists = eduService.GetAllPlaylists();

            model.ActivePlaylistId = id;

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}