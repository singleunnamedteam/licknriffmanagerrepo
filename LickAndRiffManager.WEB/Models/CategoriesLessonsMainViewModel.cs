﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.Models
{
    internal sealed class CategoriesLessonsMainViewModel
    {
        public String PlaylistId { get; set; }
        public List<CategoryViewModel> Categories { get; set; }
    }
}
