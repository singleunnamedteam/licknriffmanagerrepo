﻿using LickAndRiffManagerBLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LickAndRiffManager.Models
{
    public class CategoryViewModel
    {
        public Int32 Id { get; set; }
        public Int32 TotalDuration { get; set; }
        public Int32 FinishedDuration { get; set; }
        public Int32 CountOfFinished { get; set; }
        public Int32 CountOfLessons { get; set; }
        public String Name { get; set; }
        public Int32 CountOfPlanned { get; set; }

        public List<LessonDTO> Lessons { get; set; }
    }
}