﻿using LickAndRiffManagerBLL.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LickAndRiffManager.Models
{
    public class PlaylistsMainModel
    {
        public String ActivePlaylistId { get; set; }
        public List<PlaylistDTO> Playlists { get; set; }
    }
}