﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LickAndRiffManager.Startup))]
namespace LickAndRiffManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
