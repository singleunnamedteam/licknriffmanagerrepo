/**
 * Created by Paul on 6/29/2016.
 */

module.filter('secondsToDateTime', [function() {
    return function(seconds) {
        return new Date(1970, 0, 1).setSeconds(seconds);
    };
}])