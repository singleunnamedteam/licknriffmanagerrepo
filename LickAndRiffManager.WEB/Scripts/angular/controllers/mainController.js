/**
 * Created by Paul on 6/29/2016.
 */

module.controller("mainController", ['$scope', '$http', '$uibModal', 'mainService', function($scope, $http, $uibModal, mainService){
    $scope.mainModel = mainService.getMainModel();
    $scope.error = mainService.getError();

    $scope.collapsedArr = mainService.getCollapsedArr();
    $scope.statistics = mainService.getStatisticsModel($scope.mainModel);

    $scope.finish = function (category, lesson) {
        if (lesson.Status != 3){
            if (lesson.Status == 2) {
                $scope.statistics.planned--;
                category.CountOfPlanned--;
            }

            lesson.Status = 3;
            $scope.statistics.finished++;
            category.FinishedDuration += lesson.Duration;
            category.CountOfFinished++;
        }
        else {
            $scope.statistics.finished--;
            category.CountOfFinished--;
            lesson.Status = 0;
            category.FinishedDuration -= lesson.Duration;
        }

        mainService.changeStatus(lesson.LessonId, lesson.Status);
    }

    $scope.plan = function (category, lesson) {
        // Status: 0 - none, 1 - failed 2 - planned, 3 - finished
        switch (lesson.Status) {
            case 3:
                $scope.statistics.planned++;
                category.CountOfPlanned++;
                $scope.statistics.finished--;
                lesson.Status = 2;
                category.FinishedDuration -= lesson.Duration;
                category.CountOfFinished--;
                break;
            case 0:
                $scope.statistics.planned++;
                category.CountOfPlanned++;
                lesson.Status = 2;
                break;
            case 2:
                $scope.statistics.planned--;
                category.CountOfPlanned--;
                lesson.Status = 0;
                break;
        }

        mainService.changeStatus(lesson.LessonId, lesson.Status);
    }

    $scope.delete = function (category, lesson) {
        $http.delete('/api/lessons/Delete?id=' + lesson.LessonId)
        .success(function (res) {
            if (res == true) {
                mainService.deleteLesson(category, lesson.LessonId);
            }
            else {
                $scope.error.content = { Message: "���� �� ��� �����" }
            }
        })
        .error(function (err) {
            $scope.error.content = { Message: err }
        });
    }

    $scope.openModal = function () {
        mainService.setError(null);
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/Scripts/angular/angular-bootstrap-ui/add-lesson-modal.html',
            controller: 'addLessonModalController',
            size: 'md',
            resolve: {
            }
        });

        modalInstance.result.then(function (model) {
            mainService.setError(null);
            if (model.Name == undefined) {
                mainService.setError(model);
            }
            else {
                mainService.addLesson(model);
            }

        }, function (model) {
            if (model != "backdrop click") {
                mainService.setError(model);
            }
        });
    };
}]);