﻿module.controller('addLessonModalController', ['$scope', '$http', '$uibModalInstance', 'mainService', function ($scope, $http, $uibModalInstance, mainService) {
    $scope.mainModel = mainService.getMainModel();
    $scope.addModel = {
        Name: '',
        Link: '',
        Status: 0,
        PlaylistId: 'PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr',
        CategoryId: 0,
        Duration: 0,
    };

    $scope.ok = function (e) {
        e.preventDefault();
        $http.post('/api/Lessons/', JSON.stringify($scope.addModel)).success(function (id) {
            if (id.id != undefined) {
                var model = {
                    Name: $scope.addModel.Name,
                    Link: $scope.addModel.Link,
                    Status: $scope.addModel.Status,
                    LessonId: id.id,
                    CategoryId: $scope.addModel.CategoryId
                }

                $uibModalInstance.close(model);
            }
            else {
                $uibModalInstance.close(id);
            }
        }).error(function (err) {
            $uibModalInstance.close(err);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
}]);