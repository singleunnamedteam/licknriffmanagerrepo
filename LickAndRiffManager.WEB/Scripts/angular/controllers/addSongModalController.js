﻿module.controller('addSongModalController', ['$scope', '$http', '$uibModalInstance', function ($scope, $http, $uibModalInstance) {
    $scope.addModel = {
        Name: '',
        Link: '',
        Status: 0,
        Kind: 1
    };

    $scope.ok = function (e) {
        e.preventDefault();
        $http.post('/api/Compositions/', JSON.stringify($scope.addModel)).success(function (id) {
            if (!isNaN(id * 1)) {
                var model = {
                    Name: $scope.addModel.Name,
                    Link: $scope.addModel.Link,
                    Status: $scope.addModel.Status,
                    CompositionId: id,
                    Kind: 1 // song
                }

                $uibModalInstance.close(model);
            }
            else {
                $uibModalInstance.close(id);
            }
        }).error(function (err) {
            $uibModalInstance.close(err);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
}]);