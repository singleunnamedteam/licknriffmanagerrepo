﻿/**
 * Created by Paul on 6/29/2016.
 */

module.controller("mainLessonsController", ['$scope', '$http', 'mainService', function ($scope, $http, mainService) {
    $scope.mainModel = mainService.getMainLessonsModel();
    $scope.error = mainService.getError();
    $scope.statistics = mainService.getStatisticsModel($scope.mainModel);

    $scope.finish = function (lesson) {
        if (lesson.Status != 3) {
            if (lesson.Status == 2) {
                $scope.statistics.planned--;
            }

            lesson.Status = 3;
            $scope.statistics.finished++;
        }
        else {
            lesson.Status = 0;
            $scope.statistics.finished--;
        }

        mainService.changeStatus(lesson.LessonId, lesson.Status);
    }

    $scope.plan = function (lesson) {
        // Status: 0 - none, 1 - failed 2 - planned, 3 - finished
        if (lesson.Status == 2) {
            lesson.Status = 0;
            $scope.statistics.planned--;
        }
        else {
            if (lesson.Status == 3) {
                $scope.statistics.finished--;
            }

            lesson.Status = 2;
            $scope.statistics.planned++;
        }

        mainService.changeStatus(lesson.LessonId, lesson.Status);
    }
}]);