﻿/**
 * Created by Paul on 6/29/2016.
 */

module.controller("mainSongsController", ['$scope', '$http', 'mainService', '$uibModal', function ($scope, $http, mainService, $uibModal) {
    $scope.mainModel = mainService.getMainSongsModel();
    $scope.error = mainService.getError();
    $scope.statistics = mainService.getStatisticsModel($scope.mainModel);

    $scope.finish = function (composition) {
        if (composition.Status != 3) {
            if (composition.Status == 2) {
                $scope.statistics.planned--;
            }

            composition.Status = 3;
            $scope.statistics.finished++;
        }
        else {
            composition.Status = 0;
            $scope.statistics.finished--;
        }

        mainService.changeCompositionStatus(composition.CompositionId, composition.Status);
    }

    $scope.plan = function (composition) {
        // Status: 0 - none, 1 - failed 2 - planned, 3 - finished
        if (composition.Status == 2) {
            composition.Status = 0;
            $scope.statistics.planned--;
        }
        else {
            if (composition.Status == 3) {
                $scope.statistics.finished--;
            }

            composition.Status = 2;
            $scope.statistics.planned++;
        }

        mainService.changeCompositionStatus(composition.CompositionId, composition.Status);
    }

    $scope.delete = function (composition) {
        $http.delete('/api/compositions/Delete?id=' + composition.CompositionId)
        .success(function (res) {
            if (res == true) {
                mainService.deleteComposition(composition.CompositionId);
            }
            else {
                $scope.error.content = { Message: "Композиция не была удалена" }
            }
        })
        .error(function (err) {
            $scope.error.content = { Message: err }
        });
    }

    $scope.openModal = function () {
        mainService.setError(null);
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: '/Scripts/angular/angular-bootstrap-ui/add-composition-modal.html',
            controller: 'addSongModalController',
            size: 'md',
            resolve: {
            }
        });

        modalInstance.result.then(function (model) {
            mainService.setError(null);
            if (model.Name == undefined) {
                mainService.setError(model);
            }
            else {
                model.Kind = 1; // song
                mainService.addComposition(model);
            }

        }, function (model) {
            if (model != "backdrop click") {
                mainService.setError(model);
            }
        });
    };
}]);