/**
 * Created by Paul on 6/30/2016.
 */

module.service('mainService', ['$http', function($http){
    mainModel = { content: null };
    error = { content: null };
    collapsedArr = {};
    statisticsModel = { finished: 0, planned: 0, total: 0 };

    function correctStatistics(model) {
        var finished = 0;
        var planned = 0;
        var total = model.content.length;

        for (var i = 0; i < model.content.length ; i++) {
            if (model.content[i].Status == 3) {
                finished++;
            }
            else {
                if (model.content[i].Status == 2) {
                    planned++;
                }
            }
        }

        statisticsModel.total = total;
        statisticsModel.finished = finished;
        statisticsModel.planned = planned;
    };

    function correctStatisticsWithCategories(model) {
        var finished = 0;
        var planned = 0;
        var total = 0;

        var temp = model.content.Categories;
        for (var i = 0; i < temp.length ; i++) {
            for (var j = 0; j < temp[i].Lessons.length; j++) {
                if (temp[i].Lessons[j].Status == 3) {
                    finished++;
                }
                else {
                    if (temp[i].Lessons[j].Status == 2) {
                        planned++;
                    }
                }
                total++;
            }
        }

        statisticsModel.total = total;
        statisticsModel.finished = finished;
        statisticsModel.planned = planned;
    };

    return {
        getMainModel: function () {
            error.content = null;

            if(mainModel.content == null){
                $http.get('/api/categories/').success(function(data){
                    mainModel.content = data;
                    var index = 0;

                    for (category in mainModel.content.Categories) {
                        index = mainModel.content.Categories[category].Id;
                        collapsedArr[index + ''] = true;
                    }
                    correctStatisticsWithCategories(mainModel);
                }).error(function (err) {
                    error.content = err;
                    return null;
                })
            }

            return mainModel;
        },

        getMainLessonsModel: function () {
            error.content = null;

            if (mainModel.content == null) {
                var url = window.location.href.split('/');
                var playlistId = url[url.length - 1];
                $http.get('/api/lessons/' + playlistId).success(function(data){
                    mainModel.content = data;
                    correctStatistics(mainModel);
                }).error(function (err) {
                    error.content = err;
                    return null;
                })
            }

            return mainModel;
        },

        getMainCompositionsModel: function () {
            error.content = null;

            if (mainModel.content == null) {
                var url = window.location.href.split('/');
                var playlistId = url[url.length - 1];
                $http.get('/api/compositions/Get?kind=COMPOSITIONS').success(function (data) {
                    mainModel.content = data;
                    correctStatistics(mainModel);
                }).error(function (err) {
                    error.content = err;
                    return null;
                })
            }

            return mainModel;
        },

        getMainSongsModel: function () {
            error.content = null;

            if (mainModel.content == null) {
                var url = window.location.href.split('/');
                var playlistId = url[url.length - 1];
                $http.get('/api/compositions/Get?kind=SONGS').success(function (data) {
                    mainModel.content = data;
                    correctStatistics(mainModel);
                }).error(function (err) {
                    error.content = err;
                    return null;
                })
            }

            return mainModel;
        },

        changeStatus: function (lessonId, newStatus) {
            error.content = null;

            $http.post('/api/lessons/PostStatus?lessonId=' + lessonId + '&newStatus=' + newStatus).success(function () {
                return true;
            }).error(function (err) {
                error.content = err;
            });

            return false;
        },

        changeCompositionStatus: function (compositionId, newStatus) {
            error.content = null;

            $http.post('/api/compositions/PostStatus?compositionId=' + compositionId + '&newStatus=' + newStatus).success(function () {
                return true;
            }).error(function (err) {
                error.content = err;
            });

            return false;
        },

        addComposition: function(model){
            mainModel.content.push(model);
            statisticsModel.total++;
        },

        deleteComposition: function(compositionId){
            for (var i = 0; i < mainModel.content.length; i++) {
                if (mainModel.content[i].CompositionId == compositionId) {
                    statisticsModel.total--;
                    if (mainModel.content[i].Status != 3) {
                        if (mainModel.content[i].Status == 2) {
                            statisticsModel.planned--;
                        }
                    }
                    else {
                        statisticsModel.finished--;
                    }
                    mainModel.content.splice(i, 1);
                }
            }
        },

        deleteLesson: function (category, lessonId) {
            for (var i = 0; i < category.Lessons.length; i++) {
                if (category.Lessons[i].LessonId == lessonId) {
                    var lesson = category.Lessons[i];

                    if (lesson.Status != 3) {
                        if (lesson.Status == 2) {
                            statisticsModel.planned--;
                            category.CountOfPlanned--;
                        }
                    }
                    else {
                        statisticsModel.finished--;
                        category.CountOfFinished--;
                        category.FinishedDuration -= lesson.Duration;
                    }

                    category.Lessons.splice(i, 1);
                    category.CountOfLessons--;
                    statisticsModel.total--;
                }
            }
        },

        getError: function () {
            return error;
        },

        setError: function (err) {
            if (err != null) {
                error.content = { Message: err };
            }
            else {
                error.content = null;
            }
        },

        getCollapsedArr: function () {
            return collapsedArr;
        },

        getStatisticsModel: function (model) {
            return statisticsModel;
        },

        addLesson: function (lesson) {
            for (var i = 0; i < mainModel.content.Categories.length; i++) {
                if (mainModel.content.Categories[i].Id == lesson.CategoryId) {
                    mainModel.content.Categories[i].Lessons.push(lesson);
                    statisticsModel.total++;
                    mainModel.content.Categories[i].CountOfLessons++;
                }
            }
        }
    }
}])