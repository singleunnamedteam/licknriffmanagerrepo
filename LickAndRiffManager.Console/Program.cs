﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LickAndRiffManager.DAL.Util;
using LickAndRiffManager.DAL.Entities;
using System.Configuration;

namespace LickAndRiffManager.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            UnitOfWork uOW = new UnitOfWork(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Files\Projects\LickAndRiffManager\LickAndRiffManager.WEB\App_Data\LickNRiffDB.mdf;Integrated Security=True");
            String exMessage = "";

            bool result = uOW.Categories.Add(new Category()
            {
                Name = "test",
            }, ref exMessage);

            uOW.Save();

            System.Console.WriteLine(result);
        }
    }
}
