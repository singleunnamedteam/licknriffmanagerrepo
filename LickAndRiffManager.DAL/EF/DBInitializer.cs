﻿using LickAndRiffManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.EF
{
    class LickNRiffDBInitializer : CreateDatabaseIfNotExists<LickNRiffContext>
    {
        protected override void Seed(LickNRiffContext db)
        {
            //CONSTRAINT[FK_dbo.Lessons_dbo.Categories_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES[dbo].[Categories]([CategoryId])
            // On delete set nulls
            db.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Lessons] DROP CONSTRAINT [FK_dbo.Lessons_dbo.Categories_CategoryId]");
            db.Database.ExecuteSqlCommand("ALTER TABLE [dbo].[Lessons] ADD CONSTRAINT [FK_dbo.Lessons_dbo.Categories_CategoryId] FOREIGN KEY (CategoryId) REFERENCES [dbo].[Categories](CategoryId) ON UPDATE NO ACTION ON DELETE SET NULL");

            // Initializing DB with data
            Category cat1 = new Category() { Name = "Exercises" };
            Category cat2 = new Category() { Name = "Percussion" };
            Category cat3 = new Category() { Name = "Spanish/Flamenco" };
            Category cat4 = new Category() { Name = "Travis Picking" };
            Category cat5 = new Category() { Name = "Blues and rock & roll" };
            Category cat6 = new Category() { Name = "Country" };
            Category cat7 = new Category() { Name = "Psychology" };
            Category cat8 = new Category() { Name = "Improvising" };
            Category cat9 = new Category() { Name = "Soloing" };
            Category cat10 = new Category() { Name = "Harmonics" };
            Category cat11 = new Category() { Name = "Other" };
            Category cat12 = new Category() { Name = "Harmonica" };

            db.Categories.Add(cat1);
            db.Categories.Add(cat2);
            db.Categories.Add(cat3);
            db.Categories.Add(cat4);
            db.Categories.Add(cat5);
            db.Categories.Add(cat6);
            db.Categories.Add(cat7);
            db.Categories.Add(cat8);
            db.Categories.Add(cat9);
            db.Categories.Add(cat10);
            db.Categories.Add(cat11);
            db.Categories.Add(cat12);

            Playlist beginnerFSPl = new Playlist()
            {
                PlaylistId = "PLiYP5lAo2Jmxv1yU5IqNZ87OTl64p9KJr",
                Name = "Beginner FS Series",
            };

            Playlist chordsPl = new Playlist()
            {
                PlaylistId = "PLiYP5lAo2Jmz6nGZiQNw8AbnM7LRaIPb9",
                Name = "Understanding Chords",
            };

            db.Playlists.Add(beginnerFSPl);
            db.Playlists.Add(chordsPl);

            db.SaveChanges();
        }
    }
}
