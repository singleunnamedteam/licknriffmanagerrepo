﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using LickAndRiffManager.DAL.Entities;
using System.Configuration;
using LickAndRiffManager.DAL.EF;
using System.ComponentModel.DataAnnotations.Schema;

namespace LickAndRiffManager.DAL.EF
{
    public class LickNRiffContext: DbContext
    {
        static LickNRiffContext()
        {
            Database.SetInitializer<LickNRiffContext>(new LickNRiffDBInitializer());
        }

        public LickNRiffContext(): base("LickNRiffConnection") { }

        public LickNRiffContext(String connectionString) : base(connectionString) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<Composition> Compositions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Ids 
            modelBuilder.Entity<Category>().HasKey<Int32>(k => k.CategoryId);
            modelBuilder.Entity<Lesson>().HasKey<String>(k => k.LessonId);
            modelBuilder.Entity<Playlist>().Property(p => p.PlaylistId)
                                           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<Playlist>().HasKey<String>(k => k.PlaylistId);
            modelBuilder.Entity<Composition>().HasKey(k => k.CompositionId);

            // Relations
            modelBuilder.Entity<Category>().HasMany(c => c.Lessons)
                                           .WithOptional(l => l.Category)
                                           .HasForeignKey(l => l.CategoryId)
                                           .WillCascadeOnDelete(false);

            modelBuilder.Entity<Playlist>().HasMany(p => p.Lessons)
                                           .WithRequired(l => l.Playlist)
                                           .HasForeignKey(l => l.PlaylistId)
                                           .WillCascadeOnDelete(true);
                                                                    

            base.OnModelCreating(modelBuilder);
        }
    }
}
