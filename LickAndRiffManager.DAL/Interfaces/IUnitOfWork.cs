﻿using LickAndRiffManager.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Category, Int32> Categories { get; }
        IRepository<Lesson, String> Lessons { get; }
        IRepository<Playlist, String> Playlists { get; }
        IRepository<Composition, Int32> Compositions { get; }

        void Save();
    }
}
