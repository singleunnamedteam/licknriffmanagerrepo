﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Interfaces
{
    public interface IRepository<T, I> where T:class
    {
        T Get(I id);
        List<T> GetAll();
        bool Add(T item, ref String exceptionMessage);
        bool Delete(I index);
        bool Update(I index, T item);
        List<T> Find(Func<T, bool> predicate);
        void Save();
    }
}
