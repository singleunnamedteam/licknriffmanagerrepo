﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LickAndRiffManager.DAL.Repositories;
using LickAndRiffManager.DAL.EF;
using LickAndRiffManager.DAL.Interfaces;
using LickAndRiffManager.DAL.Entities;

namespace LickAndRiffManager.DAL.Util
{
    public class UnitOfWork: IUnitOfWork
    {
        private LickNRiffContext db;
        private CategoryRepository categoryRepo;
        private LessonRepository lessonRepo;
        private PlaylistRepository playlistRepo;
        private CompositionRepository compRepo;

        public UnitOfWork()
        {
            db = new LickNRiffContext();
        }

        public UnitOfWork(String connectionString)
        {
            db = new LickNRiffContext(connectionString);
        }


        public IRepository<Category, Int32> Categories
        {
            get
            {
                if (categoryRepo == null)
                {
                    categoryRepo = new CategoryRepository(db);
                }

                return categoryRepo;
            }
        }

        public IRepository<Lesson, String> Lessons
        {
            get
            {
                if (lessonRepo == null)
                {
                    lessonRepo = new LessonRepository(db);
                }

                return lessonRepo;
            }
        }

        public IRepository<Playlist, String> Playlists
        {
            get
            {
                if (playlistRepo == null)
                {
                    playlistRepo = new PlaylistRepository(db);
                }

                return playlistRepo;
            }
        }

        public IRepository<Composition, int> Compositions
        {
            get
            {
                if (compRepo == null)
                {
                    compRepo = new CompositionRepository(db);
                }

                return compRepo;
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            db.SaveChanges();
        }
    }
}
