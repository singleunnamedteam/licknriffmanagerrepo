﻿using LickAndRiffManager.DAL.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Entities
{
    public class Composition
    {
        public Int32 CompositionId { get; set; }
        public String Name { get; set; }
        public String Link { get; set; }
        public StatusEnum Status { get; set; }
        public KindEnum Kind { get; set; }
    }
}
