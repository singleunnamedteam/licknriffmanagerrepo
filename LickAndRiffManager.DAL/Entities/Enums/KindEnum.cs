﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Entities.Enums
{
    public enum KindEnum
    {
        COMPOSITION,
        SONG
    }
}
