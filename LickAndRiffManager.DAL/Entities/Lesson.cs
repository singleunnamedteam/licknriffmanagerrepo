﻿using System;

using LickAndRiffManager.DAL.Entities.Enums;

namespace LickAndRiffManager.DAL.Entities
{
    public class Lesson
    {
        public String LessonId { get; set; }
        public String Name { get; set; }
        public String Link { get; set; }
        public Int32 Duration { get; set; }
        public StatusEnum Status { get; set; }
        public Int32 Position { get; set; }

        // FK
        public Int32? CategoryId { get; set; }
        public String PlaylistId { get; set; }

        // Relations
        public virtual Category Category { get; set; }
        public virtual Playlist Playlist { get; set; }
    }
}
