﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Entities
{
    public class Playlist
    {
        public String PlaylistId { get; set; }
        public String Name { get; set; }

        public virtual List<Lesson> Lessons { get; set; }
    }
}
