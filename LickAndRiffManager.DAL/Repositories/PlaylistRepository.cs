﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

using LickAndRiffManager.DAL.Interfaces;
using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.EF;

namespace LickAndRiffManager.DAL.Repositories
{
    public class PlaylistRepository : IRepository<Playlist, String>
    {
        LickNRiffContext db;

        public PlaylistRepository()
        {
            db = new LickNRiffContext();
        }

        public PlaylistRepository(LickNRiffContext context)
        {
            db = context;
        }

        public PlaylistRepository(String connectionString)
        {
            db = new LickNRiffContext(connectionString);
        }


        public bool Add(Playlist item, ref String exceptionMessage)
        {
            try
            {
                db.Playlists.Add(item);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool Delete(String index)
        {
            Playlist playlist = db.Playlists.Find(index);

            if (playlist == null)
            {
                return false;
            }

            db.Playlists.Remove(playlist);
            return true;
        }

        public List<Playlist> Find(Func<Playlist, bool> predicate)
        {
            return db.Playlists.Where(predicate).ToList();
        }

        public Playlist Get(String id)
        {
            return db.Playlists.Find(id);
        }

        public List<Playlist> GetAll()
        {
            return db.Playlists.ToList();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public bool Update(String index, Playlist item)
        {
            Playlist oldPlaylist = db.Playlists.Find(index);

            if (oldPlaylist == null)
            {
                return false;
            }

            oldPlaylist.Name = item.Name;
            // Link should be immutable

            db.Entry(oldPlaylist).State = EntityState.Modified;
            return true;
        }
    }
}
