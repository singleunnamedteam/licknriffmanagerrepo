﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

using LickAndRiffManager.DAL.Interfaces;
using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.EF;

namespace LickAndRiffManager.DAL.Repositories
{
    public class LessonRepository : IRepository<Lesson, String>
    {
        LickNRiffContext db;

        public LessonRepository()
        {
            db = new LickNRiffContext();
        }

        public LessonRepository(LickNRiffContext context)
        {
            db = context;
        }

        public LessonRepository(String connectionString)
        {
            db = new LickNRiffContext(connectionString);
        }


        public bool Add(Lesson item, ref String exceptionMessage)
        {
            try
            {
                db.Lessons.Add(item);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool Delete(String index)
        {
            Lesson lesson = db.Lessons.Find(index);

            if (lesson == null)
            {
                return false;
            }

            db.Lessons.Remove(lesson);
            return true;
        }

        public List<Lesson> Find(Func<Lesson, bool> predicate)
        {
            return db.Lessons.Where(predicate).ToList();
        }

        public Lesson Get(String id)
        {
            return db.Lessons.Find(id);
        }

        public List<Lesson> GetAll()
        {
            return db.Lessons.ToList();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public bool Update(String index, Lesson item)
        {
            Lesson oldLesson = db.Lessons.Find(index);

            if (oldLesson == null)
            {
                return false;
            }

            oldLesson.Duration = item.Duration;
            oldLesson.Name = item.Name;
            oldLesson.Status = item.Status;

            db.Entry(oldLesson).State = EntityState.Modified;
            return true;
        }
    }
}
