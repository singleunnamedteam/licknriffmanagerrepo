﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Interfaces;
using LickAndRiffManager.DAL.EF;

namespace LickAndRiffManager.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category, Int32>
    {
        LickNRiffContext db;

        public CategoryRepository()
        {
            db = new LickNRiffContext();
        }

        public CategoryRepository(LickNRiffContext context)
        {
            db = context;
        }

        public CategoryRepository(String connectionString)
        {
            db = new LickNRiffContext(connectionString);
        }

        public bool Add(Category item, ref String exceptionMessage)
        {
            try
            {
                db.Categories.Add(item);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool Delete(int index)
        {
            Category item = db.Categories.Find(index);
            if (item == null)
            {
                return false;
            }

            db.Categories.Remove(item);
            return true;
        }

        public Category Get(int id)
        {
            return db.Categories.Find(id);
        }

        public List<Category> GetAll()
        {
            return db.Categories.ToList();
        }

        public List<Category> Find(Func<Category, bool> predicate)
        {
            return db.Categories.Where(predicate).ToList();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public bool Update(int index, Category item)
        {
            Category oldItem = db.Categories.Find(index);
            if (oldItem == null)
            {
                return false;
            }

            oldItem.Name = item.Name;

            db.Entry(oldItem).State = EntityState.Modified;
            return true;
        }
    }
}
