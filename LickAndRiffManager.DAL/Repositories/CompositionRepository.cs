﻿using LickAndRiffManager.DAL.EF;
using LickAndRiffManager.DAL.Entities;
using LickAndRiffManager.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LickAndRiffManager.DAL.Repositories
{
    public class CompositionRepository : IRepository<Composition, Int32>
    {
        LickNRiffContext db;

        public CompositionRepository()
        {
            db = new LickNRiffContext();
        }

        public CompositionRepository(LickNRiffContext context)
        {
            db = context;
        }

        public CompositionRepository(String connectionString)
        {
            db = new LickNRiffContext(connectionString);
        }


        public bool Add(Composition item, ref String exceptionMessage)
        {
            try
            {
                db.Compositions.Add(item);
            }
            catch (Exception ex)
            {
                exceptionMessage = ex.Message;
                return false;
            }

            return true;
        }

        public bool Delete(Int32 index)
        {
            Composition Composition = db.Compositions.Find(index);

            if (Composition == null)
            {
                return false;
            }

            db.Compositions.Remove(Composition);
            return true;
        }

        public List<Composition> Find(Func<Composition, bool> predicate)
        {
            return db.Compositions.Where(predicate).ToList();
        }

        public Composition Get(Int32 id)
        {
            return db.Compositions.Find(id);
        }

        public List<Composition> GetAll()
        {
            return db.Compositions.ToList();
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public bool Update(Int32 index, Composition item)
        {
            Composition oldComposition = db.Compositions.Find(index);

            if (oldComposition == null)
            {
                return false;
            }

            oldComposition.Name = item.Name;
            oldComposition.Status = item.Status;
            // Link should be immutable

            db.Entry(oldComposition).State = EntityState.Modified;
            return true;
        }
    }
}
